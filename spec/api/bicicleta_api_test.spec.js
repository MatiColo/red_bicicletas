var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas"

describe('Bicicletas API', ()=>{
    beforeEach(function (done) {
        var mongoDB = "mongodb://localhost/red_bicicletas";
        mongoose.connect(mongoDB, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true 
        });
        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "MongoDB conecction error: "));
        db.once("open", function () {
          console.log("You are connected to test DB");
          done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          mongoose.disconnect(err);
          done();
        });
    });

    describe('GET Bicicletas /', ()=>{
        it('Status 200', (done)=>{
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
            });
        });
    });

    describe('POST Bicicleta /create', ()=>{
        it('Status 200', (done)=>{
            var headers = {"Content-type" : "application/json"}
            var aBici = '{"id":10, "color":"rojo", "modelo":"urbana", "lat":-34, "lng": -54}'
            
            request.post({
                headers: headers,
                url: base_url,
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.modelo).toBe("urbana");
                expect(bici.ubicacion[0]).toBe("-34");
                expect(bici.ubicacion[1]).toBe("-54");
                done();
            });
        });
    });

    describe('DELETE Bicicleta /delete', ()=>{
        it('Status 204', (done)=>{
            var a = Bicicleta.createInstance(1, "negro", "urbana", [0.348955, -78.132329]);
            Bicicleta.add(a, function(){
                var headers = {"Content-type" : "application/json"}
            });
                        
            request.delete({
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: '{ "id": 1}'
            }, function(error, response, body){
                expect(response.statusCode).toBe(204);     
                done();
            })            
        });
    });
})



/*
const { bicicleta_list } = require('../../controllers/api/bicicletaControllerAPI'); 

beforeEach(()=> {bicicleta_list.allBicis = []})
describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana', [-34.6012424,-58.3861497]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
});

describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": 9.9365915, "lng": -84.1080593}';
            request.post({
                headers: headers,
                url:    'http://localhost:5000/api/bicicletas/create',
                body:    aBici 
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });
    

    describe('DELETE Bicicleta /delete', ()=>{
        it('Status 204', (done)=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var headers = {"Content-type" : "application/json"}
            var aBici = new Bicicleta(1, "rojo", "urbana", [0.348955, -78.132329]);
            Bicicleta.add(aBici);
            expect(Bicicleta.allBicis.length).toBe(1);
            
            request.delete({
                url: 'http://localhost:5000/api/bicicletas/delete',
                body: '{ "id": 1}'
            }, function(error, response, body){
                expect(response.statusCode).toBe(204);     
                done();
            })            
        });
    });

    describe('PUT Bicicleta /update', ()=>{
        it('Status 200', (done)=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var aBici = new Bicicleta(1, "rojo", "urbana", [0.348955, -78.132329]);
            Bicicleta.add(aBici);
            expect(Bicicleta.allBicis.length).toBe(1);
            
            var headers = {"Content-type" : "application/json"}
            var aBici2 = '{"id": 1, "color": "negro", "modelo": "montaña","ubicacion": [0.347249, -78.130199]}'
            request.put({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/update',
                body: aBici2
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("negro");
                expect(Bicicleta.findById(1).modelo).toBe("montaña");
                done();
            });
        });
    });
*/
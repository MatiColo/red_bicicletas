var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){   
    Bicicleta.find({}, function(err, bicicleta){
        res.status(200).json({
            bicicletas: bicicleta
        });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta({ code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});

    console.log
    Bicicleta.add(bici, function (err) {
        res.status(200).json({ bicicleta: bici });
    })
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.code);
    res.status(204).send();
}